/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema Definition
 */

const userSchema = new Schema({
    firstName: Schema.Types.String,
    lastName: Schema.Types.String,
    email: Schema.Types.String
});

/**
 * Export Schema
 */
module.exports = mongoose.model('user', userSchema);