/**
 * Import External Libraries
 */
let builder = require('botbuilder');
let mongoose = require('mongoose');
let express = require('express');
const mongo = require('mongodb').MongoClient;


/**
 * Define Bot Storage
 */
const MongoBotStorage = require('botbuilder-storage').MongoBotStorage;
let dbURL = process.env.MONGODB_URL || 'mongodb://127.0.0.1:27017/myDB';
const dbHost = dbURL.substring(0, dbURL.lastIndexOf('/'));

let router = express.Router();

/**
 * Models
 */

const User = mongoose.model('user');

/**
 * Services
 */
const Logger = require('../../services/logger');

/**
 * Initialize and configure bot
 */
const globalConfigs = {};
let connector = new builder.ChatConnector({
    // appId: process.env.MICROSOFT_APP_ID,
    // appPassword: process.env.MICROSOFT_APP_PASSWORD
});

router.get('/messages', (req, res) => {
    res.send('Hi');
});
router.post('/messages', connector.listen());


let bot = new builder.UniversalBot(connector, [(session, args, next) => {
    session.send("Hello There");
    session.beginDialog('speech');
},
    (session, args, next) => {
        session.send("Thank you!!");
    }]);


/**
 * Import and Register other Dialogs
 */

const greetingsDialog = require('./dialogs/greetings.dialog');
greetingsDialog.registerDialog(bot, globalConfigs);

const marriedDialog = require('./dialogs/Married.dialog');
marriedDialog.registerDialog(bot, globalConfigs);


const otherLawDialog = require('./dialogs/otherLaw.dialog');
otherLawDialog.registerDialog(bot, globalConfigs);

const familyDialog = require('./dialogs/Family.dialog');
familyDialog.registerDialog(bot, globalConfigs);

const heroCardDialog = require('./dialogs/heroCard.dialog');
heroCardDialog.registerDialog(bot, globalConfigs);

const videoCardDialog = require('./dialogs/videoCard.dialog');
videoCardDialog.registerDialog(bot, globalConfigs);

const speechDialog = require('./dialogs/speech.dialog');
speechDialog.registerDialog(bot, globalConfigs);

/**
 * MongoDB Session Storage
 */

mongo.connect(dbHost, (err, client) => {
    if (err) throw err;
    const setting = {collection: 'UserData'};
    client = client.db('BotStorage');
    const adapter = new MongoBotStorage(client, setting);
    bot.set('storage', adapter);
});

module.exports = router;