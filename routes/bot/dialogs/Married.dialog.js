/**
 * Import Libraries if needed
 */
let globalConfig;
let botbulider=require('botbuilder')

/**
 * Register your dialog's steps
 */
const married = [(session, args, next) => {
    botbulider.Prompts.choice(session,"Have you separated from your partner?",'In Process|Divorced',{listStyle:botbulider.ListStyle.button});
},
    (session,result,next)=>{
      let choice=result.response.entity;
        if(choice==='In Process'){
                botbulider.Prompts.choice(session,'Are there anything you want to discuss regarding children under 18?','Yes|No',{listStyle:botbulider.ListStyle.button})
                next();

        }else {
            session.beginDialog('otherLaw')
        }

    },
    (session,result,next)=>{
        if(result.response.entity==='Yes'){
            botbulider.Prompts.choice(session,'Are you happy to share your children detail? Our entire conversation will be completely confidential and secure.','Yes|No',{listStyle:botbulider.ListStyle.button})
            next();
        }else {
            session.beginDialog('married')
        }
    },
    (session,result,next)=>{
        if(result.response.entity==='Yes'){
            // botbulider.Prompts.number(session,'Could you start with your child age?');
            next()
        }else {
            session.beginDialog('married')
        }
    },
    (session, args, next) => {

    }];

/**
 * Must export this 1 method which registers your dialog.
 */
module.exports = {
    registerDialog: (bot, config) => {
        globalConfig = config;
        bot.dialog('married', married);
    }
};
