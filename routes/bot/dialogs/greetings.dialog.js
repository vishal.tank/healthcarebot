/**
 * Import Libraries if needed
 */
let globalConfig;
let botbulider=require('botbuilder')

/**
 * Register your dialog's steps
 */
const greetingsDialogSteps = [(session, args, next) => {
    botbulider.Prompts.choice(session,"Which topic we talk",'Family Law|Other Law',{listStyle:botbulider.ListStyle.button});
},
    (session,result)=>{
      let choice=result.response.entity;
        if(choice==='Family Law'){
            session.beginDialog('FamilyLaw');

        }else {
            session.beginDialog('otherLaw')
        }

    },
    (session, args, next) => {
        session.endDialog("Than you for coming in greetings dialog");
    }];


/**
 * Must export this 1 method which registers your dialog.
 */
module.exports = {
    registerDialog: (bot, config) => {
        globalConfig = config;
        bot.dialog('greetings', greetingsDialogSteps);
    }
};
