let botBuilder = require('botbuilder');
let globalConfig;

const videoCardFlowSteps = [
    (session, args, next) => {
        var msg = new botBuilder.Message(session);
        msg.attachmentLayout(botBuilder.AttachmentLayout.carousel)
        msg.attachments([
            new botBuilder.VideoCard(session)
                .title("Classic White T-Shirt")
                .subtitle("100% Soft and Luxurious Cotton")
                .text("Price is $25 and carried in sizes (S, M, L, and XL)")
                .media([{ url: '/home/kevit/Desktop/VishalTank/botframework/public/videoplayback.mp4' }])
                .image([botBuilder.CardImage.create(session, '/home/kevit/Desktop/VishalTank/botframework/public/unnamed.png')])
                .buttons([
                    botBuilder.CardAction.imBack(session, "buy classic white t-shirt", "Buy")
                ])
        ])

        session.send(msg).endDialog();
    }];


module.exports = {
    registerDialog: (bot, config) => {
        globalConfig = config;
        bot.dialog('videoCard', videoCardFlowSteps);
    }
};