let botbuilder=require('botbuilder');
let globalConfig;
const User=require('../../../models/user.model')
const FamilyLaw = [(session, args, next) => {
        session.send("you are in Family Law");
    // session.beginDialog()
    next();
},
    (session,result,next)=>{
    session.send('Can I organise a free confidential consultation with one of our family law lawyers to assist you');
    session.send('Our entire conversation will be completely confidential and this point we are collecting some key information to assist you. At this point, we will not provide you legal advice but this information gets you most out of the initial consultation');
    botbuilder.Prompts.choice(session,'Would you like to get started?','I would like to continue...|I’m not ready yet?',{listStyle:botbuilder.ListStyle.button});
    },
    (session,result,next)=>{
            let choice=result.response.entity;
            if(choice==='I would like to continue...'){
                next()
            }else {
                session.replaceDialog('greetings')
            }
    },
    (session)=>{
        botbuilder.Prompts.text(session,"That’s a great start! Can I start with your first name?");
    },
    (session,result)=>{
        botbuilder.Prompts.text(session,'I would like to know your last name also.');
        session.userData.profile={}
        session.userData.profile['firstName']=result.response;

    },
    (session,result)=>{
       botbuilder.Prompts.text(session,'Can I have your email address?');
        session.userData.profile['lastName']=result.response;
    },
    (session,result)=>{
        botbuilder.Prompts.choice(session,'Tell us about your current relationship status?','Married|conflict',{listStyle:botbuilder.ListStyle.button})
        session.userData.profile['email']=result.response;
        let user=new User(session.userData.profile);
        user.save().then(r=>{
            console.log("Data saved ",r)
        }).catch(e=>{
            console.log(e)
        })
    },
    (session,result)=>{
        let choice=result.response.entity;
        if(choice==='Married'){
            session.beginDialog('married');
        }else {
            session.beginDialog('conflict');
        }

    },
    (session, args, next) => {
        session.endDialog("Than you for coming in Family dialog");
    }];

module.exports = {
    registerDialog: (bot, config) => {
        globalConfig = config;
        bot.dialog('FamilyLaw', FamilyLaw);
    }
};
