let botBuilder = require('botbuilder');
let globalConfig;

const heroCardFlowSteps = [
    (session, args, next) => {
        let msg = new botBuilder.Message(session);
        msg.attachmentLayout(botBuilder.AttachmentLayout.carousel)
        msg.attachments([
            new botBuilder.HeroCard(session)
                .title("Classic White T-Shirt")
                .subtitle("100% Soft and Luxurious Cotton")
                .text("Price is $25 and carried in sizes (S, M, L, and XL)")
                .images([botBuilder.CardImage.create(session, '/home/kevit/Desktop/VishalTank/botframework/public/Destop.jpg')])
                .buttons([
                    botBuilder.CardAction.imBack(session, "buy classic white t-shirt", "Buy")
                ]),
            new botBuilder.HeroCard(session)
                .title("Classic Gray T-Shirt")
                .subtitle("100% Soft and Luxurious Cotton")
                .text("Price is $25 and carried in sizes (S, M, L, and XL)")
                .images([botBuilder.CardImage.create(session, '/home/kevit/Desktop/VishalTank/botframework/public/unnamed.png')])
                .buttons([
                    botBuilder.CardAction.imBack(session, "buy classic gray t-shirt", "Buy")
                ])
        ]);
        session.send(msg);
        // session.beginDialog('videoCard');
        next();
    },
    (session, args, next) => {
        botBuilder.Prompts.text(session, 'Type something...');
        next();
    },
    (session, result, next) => {
        if(result.response)
            session.beginDialog('greetings');
    }
];


module.exports = {
    registerDialog: (bot, config) => {
        globalConfig = config;
        bot.dialog('heroCard', heroCardFlowSteps);
    }
};